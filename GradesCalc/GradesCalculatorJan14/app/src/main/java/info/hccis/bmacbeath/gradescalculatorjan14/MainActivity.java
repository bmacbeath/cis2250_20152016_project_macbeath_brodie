package info.hccis.bmacbeath.gradescalculatorjan14;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonCalculate = null;
    private EditText projectGrade = null;
    private EditText researchGrade = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Associate with the widgets.
        projectGrade = (EditText) findViewById(R.id.projectGrade);
        researchGrade = (EditText) findViewById(R.id.researchGrade);
        buttonCalculate = (Button) findViewById((R.id.buttonCalculate));

        buttonCalculate.setOnClickListener(
            new View.OnClickListener(){
                public void onClick(View view){

                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;

                    if(projectGrade.getText().toString().equals("")){
                        CharSequence text = "Please enter a grade for your project";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    } else if (researchGrade.getText().toString().equals("")){
                        CharSequence text2 = "Please enter a grade for your research";
                        Toast toast2 = Toast.makeText(context, text2, duration);
                        toast2.show();
                    } else {
                        Log.d("CALCULATE", "The calculate button was clicked");
                        Log.d("CALCULATE", "The project grade entered was: " + researchGrade.getText().toString());
                        Log.d("CALCULATE", "The project grade entered was: " + projectGrade.getText().toString());
                        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                        intent.putExtra("ProjectGrade", projectGrade.getText().toString());
                        intent.putExtra("ResearchGrade", researchGrade.getText().toString());
                        startActivity(intent);
                    }
                }
            }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_restart) {
            Log.d("RESTART_MENU", "The restart menu item was chosen");
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
