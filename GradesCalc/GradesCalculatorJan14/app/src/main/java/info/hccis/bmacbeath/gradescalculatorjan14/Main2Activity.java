package info.hccis.bmacbeath.gradescalculatorjan14;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Main2Activity extends AppCompatActivity {

    private TextView textOverallGrade = null;
    private Button buttonReset = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Associate with the widgets.
        textOverallGrade = (TextView) findViewById(R.id.textOverallGrade);
        buttonReset = (Button) findViewById(R.id.buttonRestart);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String projectGrade = extras.getString("ProjectGrade");
            String researchGrade = extras.getString("ResearchGrade");
            double grade = (Integer.parseInt(projectGrade) * 0.8) + (Integer.parseInt(researchGrade) * 0.2);

            DecimalFormat precision = new DecimalFormat("0.00");

            Log.d("CALCULATE", "The overall grade is: " + precision.format(grade) + "%");

            textOverallGrade.setText("The overall grade is: " + precision.format(grade) + "%");
        }

        buttonReset.setOnClickListener(new View.OnClickListener() {
                                           public void onClick(View view) {
                                               Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                                               startActivity(intent);
                                           }
                                       }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_restart) {
            Log.d("RESTART_MENU", "The restart menu item was chosen");
            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}