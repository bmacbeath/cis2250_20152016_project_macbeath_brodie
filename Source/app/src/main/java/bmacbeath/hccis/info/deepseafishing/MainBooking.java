package bmacbeath.hccis.info.deepseafishing;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import bmacbeath.hccis.info.deepseafishing.dao.BookingsDAO;

/**
 * This main booking class holds the main functionality for the program, including validation,
 * checking internet connection and adding the informaiton to the database/server or the local
 * phone server files.
 *
 * @author Brodie MacBeath
 * @since 2016-02-12
 */
public class MainBooking extends AppCompatActivity {

    //Initializing variables
    private EditText firstName = null;
    private EditText lastName = null;
    private EditText phoneNumber = null;
    private EditText email = null;
    private EditText dateOfTrip = null;
    private Spinner timeOfTrip = null;
    private EditText creditCardNumber = null;
    private Spinner numberOfPassengers = null;
    private Button buttonAddBooking = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_booking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(checkInternetConnection()){
            BookingsDAO bookDao = new BookingsDAO(this);

            if(bookDao.checkDatabase()){
                try {
                    String pass = bookDao.updateDatabase();

                    //checks to make sure that the response was not empty and not 500
                    //if it was not then tell the user that there booking has been added
                    //and allow them to share their new booking.
                    if(!pass.isEmpty() && !pass.equals("500")) {
                        //let the user know that their booking has been successfully made
                        CharSequence text = "Your booking that you have made previously has been successfully made.";
                        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                        toast.show();
                        bookDao.clearTable();
                    } else {
                        CharSequence text = "Your booking that you have made previously has not been successfully made. Please try again later.";
                        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    bookDao.closeConnection();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //Associate the variables with the widgets.
        firstName = (EditText) findViewById(R.id.txtFirstName);
        lastName = (EditText) findViewById(R.id.txtLastName);
        phoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
        email = (EditText) findViewById(R.id.txtEmail);
        dateOfTrip = (EditText) findViewById(R.id.txtDate);
        timeOfTrip = (Spinner) findViewById(R.id.timeOfTrip);
        creditCardNumber = (EditText) findViewById(R.id.txtCreditCardNumber);
        numberOfPassengers = (Spinner) findViewById(R.id.numberOfPassengers);
        buttonAddBooking = (Button) findViewById(R.id.buttonAddBooking);

        //when the add booking button is clicked, check to make sure all the fields are filled out
        //correctly and check to make sure that there is an internet connection and if there isn't
        //store the database information locally on the device.
        buttonAddBooking.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View view) {

                    //Setting variables for the toast's
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_LONG;

                    //checking to make sure the first name field is not empty
                    if(firstName.getText().toString().equals("")){
                        CharSequence text = "Please enter your first name";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the last name field is not empty
                    } else if(lastName.getText().toString().equals("")){
                        CharSequence text = "Please enter your last name";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the phone number field is not empty
                    } else if(phoneNumber.getText().toString().equals("")){
                        CharSequence text = "Please enter your phone number";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the phone number entered is 10 digits long
                    } else if(phoneNumber.getText().length() != 10) {
                        CharSequence text = "Please make sure your phone number is 10 characters long";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the email field is not empty
                    } else if(email.getText().toString().trim().equals("")){
                        CharSequence text = "Please enter your email";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the email entered is a valid email
                    } else if(!isValidEmail(email.getText().toString().trim())){
                        CharSequence text = "Please enter a valid email address";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the date of the trip field is not empty
                    } else if(dateOfTrip.getText().toString().equals("")) {
                        CharSequence text = "Please enter the date of your trip";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure that the date of the trip is in the correct format
                    } else if (!isValidFormat(dateOfTrip.getText().toString())){
                        CharSequence text = "Please make sure that the date is yyyy-mm-dd";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure that the date entered is a date in the future
                    } else if(DateIsOlder(dateOfTrip.getText().toString())) {
                        CharSequence text = "Please select a date in the future";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure that the time of the trip field spinner has been changed
                    //and that a valid time has been selected
                    } else if(timeOfTrip.getSelectedItem().toString().equals("Time Of Trip")){
                        CharSequence text = "Please enter the time of your trip";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure that the credit card number field is not empty
                    } else if(creditCardNumber.getText().toString().equals("")){
                        CharSequence text = "Please enter your credit card number";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure the credit card number entered is 16 digits long
                    } else if(creditCardNumber.getText().length() != 16){
                        CharSequence text = "Your credit card number must be 16 characters";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    //checking to make sure that the number of passengers field spinner has been changed
                    //and that a valid number of passengers has been selected
                    } else if(numberOfPassengers.getSelectedItem().toString().equals("Number Of Passengers")){
                        CharSequence text = "Please enter the number of passengers";
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    } else {
                        //the link to connect to the web service
                        String link = "http://bjmac.hccis.info:8080/deepseafishing/rest/dataaccess/addbooking/";
                        String url = link;

                        //Convert the edit text information that the user entered into strings.
                        String firstNameEntered = String.valueOf(firstName.getText());
                        String lastNameEntered = String.valueOf(lastName.getText());
                        String phoneNumberEntered = String.valueOf(phoneNumber.getText());
                        String emailEntered = String.valueOf(email.getText());
                        final String dateOfTripEntered = String.valueOf(dateOfTrip.getText());
                        String timeOfTripEntered = String.valueOf(timeOfTrip.getSelectedItem().toString());
                        String creditCardNumberEntered = String.valueOf(creditCardNumber.getText());
                        String numberOfPassengersEntered = String.valueOf(numberOfPassengers.getSelectedItem().toString());

                        //create a valid time of trip variable to hold the correct time format for
                        //passing the information to the database
                        String validTimeOfTrip = null;

                        //if statement checking what the user selected and assigning a variable
                        //to hold the correct time format for the web service
                        if(timeOfTripEntered.equals("8 AM")){
                            validTimeOfTrip = "08:00";
                        } else if (timeOfTripEntered.equals("1 PM")){
                            validTimeOfTrip = "13:00";
                        } else if (timeOfTripEntered.equals("6 PM")){
                            validTimeOfTrip = "18:00";
                        }

                        //adding the information to insert the booking information to the web service
                        url += URLEncoder.encode(creditCardNumberEntered + "," + lastNameEntered + "," + firstNameEntered + "," + phoneNumberEntered + "," + emailEntered + "," + dateOfTripEntered + "," + validTimeOfTrip + "," + numberOfPassengersEntered);

                        //checks to make sure that there is an internet connection. If there is then
                        //continue and add the information through the web service. If there is not
                        //a internet connection, then use SQLite to hold the database information
                        //and push it to the database when the user gets an internet connection.
                        if(checkInternetConnection()){

                            //starting the new connection to the web service
                            Connection connection = new Connection(url);
                            connection.execute("");

                            try {
                                //get connection response
                                String pass = connection.get();

                                //checks to make sure that the response was not empty and not 500
                                //if it was not then tell the user that there booking has been added
                                //and allow them to share their new booking.
                                if(!pass.isEmpty() && !pass.equals("500")) {
                                    //let the user know that their booking has been successfully made
                                    CharSequence text = "Your booking has successfully been made.";
                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();

                                    //starts a media player object to allow a audio file to be played
                                    //when the user successfully books a fishing trip
                                    MediaPlayer mp = MediaPlayer.create(MainBooking.this, R.raw.fishing_pole_with_splash);
                                    mp.start();

                                    //starts a new intent to allow the user to share their booking
                                    //on social media, if they would like
                                    Intent shareIntent = new Intent();
                                    shareIntent.setAction(Intent.ACTION_SEND);
                                    shareIntent.putExtra(Intent.EXTRA_TEXT, "I have booked a deep sea fishing trip on " + dateOfTripEntered + " at " + timeOfTripEntered);
                                    shareIntent.setType("text/plain");
                                    startActivity(Intent.createChooser(shareIntent, "Share this booking using ..."));
                                } else {
                                    //if there was an error adding the information into the database
                                    //let the user know there was an error
                                    CharSequence text = "There was an error in your booking. Please try again later.";
                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //starts a media player object to allow a audio file to be played
                            //when the user successfully books a fishing trip
                            MediaPlayer mp = MediaPlayer.create(MainBooking.this, R.raw.fishing_pole_with_splash);
                            mp.start();

                            //let the user know the booking was created successfully and once they
                            //connect to the internet, their booking will be added to the database
                            CharSequence text = "Your booking has successfully been made. When you are connected to the internet, your booking will be submitted to the deep sea fishing company.";
                            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                            toast.show();

                            //do the SQLite stuff here
                            BookingsDAO bookDao = new BookingsDAO(MainBooking.this);

                            bookDao.insertBooking(url);
                            bookDao.getBookings();
                            bookDao.closeConnection();
                        }
                    }
                }
            }
        );
    }

    /**
     * This method checks the internet connection on the android device and returns a true or false
     * response depending on whether or not the user has an internet connection.
     *
     * @return true/false
     *
     * @author http://stackoverflow.com/questions/13662251/how-to-know-whether-device-has-data-connection
     * @updated Brodie MacBeath
     * @since 2016-02-12
     */
    private boolean checkInternetConnection() {
        boolean hasInternetConnection = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    hasInternetConnection = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    hasInternetConnection = true;
        }
        return hasInternetConnection;
    }

    /**
     * This method checks to see if the date passed in through the param, is in the past or the
     * future. This is used to make sure that the user passes in a valid date in the future for
     * a deep sea fishing trip.
     *
     * @param date
     * @return isOlder
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public boolean DateIsOlder(String date) {
        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
        boolean isOlder = false;

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        String currentDate = year + "-" + month + "-" + day;

        try{
            if(dfDate.parse(String.valueOf(date)).before(dfDate.parse(currentDate))){
                isOlder = true;
            }
            else {
                isOlder = false;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return isOlder;
    }

    /**
     * This method checks and validates an email that is passed in through the params to make sure
     * it is a valid email address.
     *
     * @param email
     * @return Pattern.compile(regex).matcher(email).matches();
     *
     * @author http://stackoverflow.com/questions/1819142/how-should-i-validate-an-e-mail-address
     * @updated Brodie MacBeath
     * @since 2016-02-12
     */
    private boolean isValidEmail(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static boolean isValidFormat(String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_booking, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_contact_us) {
            Intent intent = new Intent(this, ContactUs.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_add_booking) {
            Intent intent = new Intent(this, MainBooking.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.youtube_video) {
            Intent intent = new Intent(this, VideoTube.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}