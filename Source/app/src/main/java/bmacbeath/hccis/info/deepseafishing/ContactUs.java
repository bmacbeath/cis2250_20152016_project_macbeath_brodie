package bmacbeath.hccis.info.deepseafishing;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.ArrayList;

/**
 * This contact us class holds the main functionality contact us page. This class allows the user
 * to click the add contact button and then the deep sea fishing contact gets added to the contacts
 * on the users device
 *
 * @author Brodie MacBeath
 * @since 2016-02-12
 */
public class ContactUs extends AppCompatActivity {

    //Initializing variables
    private Button contactUsButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Associate the variables with the widgets.
        contactUsButton = (Button) findViewById(R.id.buttonAddContact);

        //when the contact us button is clicked, it adds a contact in the users phone with
        //deep sea fishing's phone number, email, and website.
        contactUsButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        if(insertContact(getContentResolver(), "Deep Sea Fishing", "123-456-7890", "richards@deepseafishing.com", "http://bjmac.hccis.info:8080/deepseafishing")){
                            //if the contact was added then the user is told that it worked
                            CharSequence text = "The Deep Sea Fishing contact has been added to your device.";
                            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            //if the contact was added then the user is shown that there was an error
                            CharSequence text = "There was an error adding the contact to your device.";
                            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                }
        );

    }


    /**
     * This method inserts the contact information to the local device storage.
     *
     * @param contactAdder
     * @param firstName
     * @param mobileNumber
     * @param email
     * @param website
     * @return true/false
     *
     * @author http://stackoverflow.com/questions/10796640/how-to-add-phone-no-email-website-etc-to-existing-contact
     * @updated Brodie MacBeath
     * @since 2016-02-12
     */
    public static boolean insertContact(ContentResolver contactAdder,
                                        String firstName, String mobileNumber, String email, String website) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(
                        ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                        firstName).build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
                        mobileNumber)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.TYPE_WORK).build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS,
                        email)
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE,
                        ContactsContract.CommonDataKinds.Email.TYPE_WORK).build());
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Website.URL,
                        website)
                .withValue(ContactsContract.CommonDataKinds.Website.TYPE,
                        ContactsContract.CommonDataKinds.Website.TYPE_WORK).build());
        try {
            contactAdder.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_booking, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_contact_us) {
            Intent intent = new Intent(this, ContactUs.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_add_booking){
            Intent intent = new Intent(this, MainBooking.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.youtube_video) {
            Intent intent = new Intent(this, VideoTube.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
