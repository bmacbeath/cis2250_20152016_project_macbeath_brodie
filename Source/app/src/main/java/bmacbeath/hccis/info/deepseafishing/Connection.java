package bmacbeath.hccis.info.deepseafishing;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/*
 * Connection class which gets data from any URL.
 *
 * What is AsyncTask?
 *
 * An background UI thread handler, which prevents the Main thread from becoming hung with large task like data access.
 * For more information: http://developer.android.com/reference/android/os/AsyncTask.html
 *
 * What is HttpURLConnection?
 *
 * Simply put it is an network connection.
 * For more information: http://developer.android.com/reference/java/net/HttpURLConnection.html
 *
 */

public class Connection extends AsyncTask<String, String, String> {

    private String link;

    public Connection(String service) {
        this.link = getLink(service);
    }

    public Connection() {
    }

    /*
     * Method performs the network connection in background. The response will be collected in calling class.
     */
    @Override
    protected String doInBackground(String... params) {

        StringBuilder result = new StringBuilder(); //sets return string
        HttpURLConnection urlConnection = null; //sets connection
        try {
            URL url = new URL(link); //create url
            urlConnection = (HttpURLConnection) url.openConnection(); //open the connection
            urlConnection.connect(); //connect to internet

            //how to handle response codes
            switch (urlConnection.getResponseCode()) { //get response code from service
                case 200: //if data found
                case 201: //if data added
                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    Log.d("TEST", "In data found/added");

                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    break;
                default: //handle all error codes
                    result.append("500"); //pass default error http error code as result and handle on activity
                    Log.d("TEST", "In error " + urlConnection.getResponseCode());
                    break;
            }

        } catch (MalformedURLException e) {
            Log.d("Malformed URL", e.toString());
        } catch (IOException e) {
            Log.d("IOException", e.toString());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect(); //close connection
            }
        }
        return result.toString(); //return results as string
    }

    @Override
    protected void onPostExecute(String results) {
        //this method is meant for testing data (can't return values)
        Log.d("Post Execute Results", results);
    }

    /*
     * This method allow user to use set links or define their own link with default option.
     */
    private String getLink(String service) {
        String link;
        switch (service) {
            default:
                link = service;
                break;
        }
        return link;
    }

    //standard getters and setters
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
