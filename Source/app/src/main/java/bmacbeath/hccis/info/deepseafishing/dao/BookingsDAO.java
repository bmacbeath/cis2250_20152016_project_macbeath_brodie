package bmacbeath.hccis.info.deepseafishing.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import bmacbeath.hccis.info.deepseafishing.Connection;
import bmacbeath.hccis.info.deepseafishing.model.Bookings;

/**
 * This class is the main functionality for using SQLite Databases
 *
 * @author Brodie MacBeath
 * @since 2016-02-12
 */
public class BookingsDAO extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "bjmac_deepseafishing";          // <=== Set to 'null' for an in-memory database
    public static final String TABLE_NAME = "DeepSeaFishingWebService";
    public static final String COLUMN_ID = "_id";           // <=== This is required
    public static final String COLUMN_NAME = "web_service_string";

    private static final int DB_VERSION = 1;                 // the version of the database

    private static final String SQL_CREATE_TABLE_DELIVERABLES = "CREATE TABLE " + TABLE_NAME + "( " +
                                                                COLUMN_NAME + " TEXT);";

    private SQLiteDatabase db;
    private Cursor cursor;

    public BookingsDAO(Context context){
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create the database
        db.execSQL(SQL_CREATE_TABLE_DELIVERABLES);

    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Helper method to insert a row into the 'Bookings' table
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public boolean insertBooking(String webServiceUrl) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            ContentValues deliverableValues = new ContentValues();

            // NAME, WEIGHT
            deliverableValues.put(COLUMN_NAME, webServiceUrl);


            db.insert(TABLE_NAME, null, deliverableValues);
            db.close();

            success = true;
        } catch ( SQLiteException ex ) {
            Log.d("BookingsDAO", "Failed to insert into the database");
            success = false;
        }

        return success;
    }

    /**
     * Clears the bookings table
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public void clearTable(){

        db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + ";");
        db.execSQL("VACUUM;");

    }

    /**
     * Updates the database
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public String updateDatabase(){
        String pass = "";
        try{
            db = this.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    String update = cursor.getString(0);

                    //starting the new connection to the web service
                    Connection connection = new Connection(update);
                    connection.execute("");

                    //get connection response
                    pass = connection.get();

                    Log.d("Updated URL", update);

                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e){
            Log.d("UPDATE", "Failed to update database");
        } catch (InterruptedException e) {
            Log.d("UPDATE", "Failed to update database");
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.d("UPDATE", "Failed to update database");
            e.printStackTrace();
        }
        return pass;
    }

    /**
     * Method to get the bookings from the database
     * @return tempBook
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public ArrayList<Bookings> getBookings(){

        ArrayList<Bookings> tempBook = new ArrayList<>();
        Bookings booking = null;
        try {
            db = this.getReadableDatabase();
            cursor = db.rawQuery("select * from " + TABLE_NAME, null);
            if (cursor.moveToFirst()) {
                do {
                    booking = new Bookings(cursor.getString(0));

                    Log.d("Bookings", booking.toString());

                    tempBook.add(booking);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e){
            Log.d("Bookings", "Failed to get bookings");
        }
        return tempBook;
    }

    /**
     * Method to close the database connection
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public void closeConnection() {

        if( cursor != null) { cursor.close(); }
        if( db != null) { db.close(); }

    }

    /**
     * Method to check to see if the database exists
     *
     * @return true/false
     *
     * @author Brodie MacBeath
     * @since 2016-02-12
     */
    public boolean checkDatabase(){
        db = this.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        if(cursor.moveToFirst()){
            return true;
        } else {
            return false;
        }
    }
}

