package bmacbeath.hccis.info.deepseafishing;

/**
 * This class is used to hold a youtube API key for embedding a YouTube video in the app
 *
 * @author Brodie MacBeath
 * @since 2016-02-12
 */
public class Config {

    private Config(){

    }

    public static final String YOUTUBE_API_KEY = "AIzaSyAWs6NlYVujHNQy4B3WlAU27xin47D9wL8";

}
