package bmacbeath.hccis.info.deepseafishing.model;


/**
 * This class is used to hold the bookings objects
 *
 * @author Brodie MacBeath
 * @since 2016-02-12
 */
public class Bookings {

    private String webServiceString;


    public Bookings(String webServiceString) {
        this.webServiceString = webServiceString;
    }

    public String getWebServiceString() {
        return webServiceString;
    }

    public void setWebServiceString(String webServiceString) {
        this.webServiceString = webServiceString;
    }
}
